import { Locator, Page } from "@playwright/test";

export class AppLayout {
	readonly page: Page;
	readonly loginEmailInput: Locator;
	readonly loginPasswordInput: Locator;

	constructor(page: Page) {
		this.page = page;
		this.loginEmailInput = this.page.locator("input#email");
		this.loginPasswordInput = this.page.locator("input#password");

		// We can get elements by other properties
		// this.loginPasswordInput = this.page.getByTestId("login-password");
		//  <div data-testid="login-password">

		// this.tablist = this.page.getByRole("tablist");
		//  <div role="tablist">
		// Roles: https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles
	}
}
