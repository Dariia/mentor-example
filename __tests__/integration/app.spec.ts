import { test, expect } from "../fixtures/base";

test("App's layout", async ({ app }) => {
	const { page } = app;

	test.afterAll(async () => {
		await page.close();
	});

	await test.step("Log in page", async () => {
		await page.goto("/login");
		await expect(page.textContent("h1")).toBe("Sign in to your account");
        await expect(page.getByText("Don’t have an account yet?")).toBeVisible();
		await expect(page.getByText("Sign up")).toHaveAttribute("href", "/signup");
	});
});
