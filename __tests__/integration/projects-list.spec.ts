import { test, expect } from "../fixtures/base";

test("App's layout", async ({ app }) => {
	const { page, projectList } = app;

	test.afterAll(async () => {
		await page.close();
	});

	await test.step("Log in", async () => {
		await app.login();
	});

	await test.step("Project list page", async () => {
		await expect(page.textContent("h1")).toBe("Projects");
		await expect(page.getByAltText("Logo")).toHaveAttribute("src", "/static/assets/delvin_icon-12d7008e.png");
		await expect(page.getByText("Logout")).toBeVisible();
		await expect(page.getByText("Your Projects")).toBeVisible();
		await expect(page.getByText("+ New Project")).toBeVisible();
	});
});
