import { test as baseTest } from "@playwright/test";
import { App } from "./app";
import { jwtToken, user } from "./user/user";
import { userProjectsList } from "./projects/projects";

const getJwtToken = /\/auth\/jwt\/create\//;
const getLoggedInUser = /\auth\/users\/me\//;
const getProjectsList = /\/api\/projects\/list\/?owner_id=ca0a5c45-8d39-45f0-8259-9d8c9adf8ec7/

export enum RequestMethod {
	Get = "GET",
	Patch = "PATCH",
	Post = "POST",
	Put = "PUT",
	Delete = "DELETE",
}

export const test = baseTest.extend<{ app: App }>({
	app: async ({ page }, use) => {
		const app = new App(page);

		await app.page.route(
			getJwtToken,
			async (route, request) => {
				await route.fulfill({
					status: 200,
					json: JSON.stringify(jwtToken),
				});
			}
		);

		await app.page.route(
			getLoggedInUser,
			async (route, request) => {
				await route.fulfill({
					status: 200,
					json: JSON.stringify(user),
				});
			}
		);

		await app.page.route(
			getProjectsList,
			async (route, request) => {
				await route.fulfill({
					status: 200,
					json: JSON.stringify(userProjectsList),
				});
			}
		);

		await use(app);
	},
});

export const expect = test.expect;
