const project1 = {
    id: "ce08188c-5739-4a80-b1c6-31a44dba7775",
    token_key: "Token33567",
    owner: "info@tangibleai.com",
    name: "test"
};

const project2 = {
    id: "b28a5958-22f0-4abb-a323-e600e2ae8767",
    token_key: "Token7c45a",
    owner: "maria@tangibleai.com",
    name: "Syndee Experiment Deterministic"
}

export const userProjectsList = [project1, project2];
