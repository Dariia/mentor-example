import { expect, Page } from "@playwright/test";
import { AppLayout } from "../pages/app-layout";
import { ProjectList } from "../pages/project-list";
import { ProjectDetails } from "../pages/project-details";

export class App {
	readonly appPage: Page;
	readonly appLayout: AppLayout;
	readonly projectList: ProjectList;
	readonly projectDetails: ProjectDetails;

	constructor(page: Page) {
		this.appPage = page;
		this.appLayout = new AppLayout(this.appPage);
		this.projectList = new ProjectList(this.appPage);
		this.projectDetails = new ProjectDetails(this.appPage);
	}

	get page() {
		return this.appPage;
	}

	async login() {
		await this.page.goto("/login");
		await expect(this.appLayout.loginEmailInput).toBeVisible();
		await this.appLayout.loginEmailInput.type("test@gmail.com");
		await this.appLayout.loginPasswordInput.type("12345");
		await this.page.getByText("Sign in").click();
		await this.page.waitForURL("/");
	}
}
