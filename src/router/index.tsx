import HomePage from "@/layouts/NewProjectPage";
import { PROJECT_PAGE_URLS } from "./pageUrls";
import { projectListLoader } from "../modules/projectsModule/loaders";
import { QueryClient } from "@tanstack/react-query";
import { axiosInstance as AxiosInstance } from "../api";
import withAuth, {PortalFeatures} from "../utils/with-auth";

const NewHomePageWithAuth = withAuth(HomePage);

const router = (
    queryClient: QueryClient,
    axiosInstance: typeof AxiosInstance | null,
    userId: number,
) => {
    return [
        {
            path: PROJECT_PAGE_URLS.project.path,
            element: <NewHomePageWithAuth/>,
            children: [
                {
                    path: PROJECT_PAGE_URLS.project.children.list,
                    lazy: () => import("src/modules/projectModule/ProjectList.tsx"),
                    loader: () =>
                        projectListLoader(queryClient, axiosInstance, userId),
                },
            ],
        },
    ]
}

export default router;

