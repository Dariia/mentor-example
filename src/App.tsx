import React from 'react';
import { Toaster } from "@/components/ui/toaster";
import store from "@/store";
import { Provider } from "react-redux";
import { RouterProvider } from "react-router-dom";
import "./App.css";
import { PAGE_URLS } from "./router";
import router from "router";
import './App.css';
import { axiosInstance } from "./api";
import {
    MutationCache,
    QueryCache,
    QueryClient,
    QueryClientProvider,
} from "react-query";
import {handleServerError} from "./utils/error";

export const STALE_QUERY_CLIENT_TIME = 1000 * 60;

const queryClient= new QueryClient({
    queryCache: new QueryCache({
        onError(error: Error) {
            handleServerError(error);
        },
    }),
    mutationCache: new MutationCache({
        onError(error: Error) {
            handleServerError(error);
        },
    }),
    defaultOptions: {
        queries: {
            staleTime: STALE_QUERY_CLIENT_TIME,
        },
    },
});

function App() {
  const userId = useSelector((state: RootState) => state.auth.user?.id) ?? "";

  return (
      <>
        <Provider store={store}>
          <QueryClientProvider client={queryClient}>
              <RouterProvider
                  router={router(PAGE_URLS, queryClient, axiosInstance, userId)}
              />
          </QueryClientProvider>
        </Provider>
        <Toaster />
      </>
  );
}

export default App;
