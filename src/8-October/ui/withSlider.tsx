import { ComponentProps } from "react";
import { useNavigate } from "react-router-dom";
import { useDisclose } from "src/components/ui/slider";
import { default as SlideOver } from "src/components/ui/slider";

export type WithSliderProps = Partial<ComponentProps<typeof SlideOver>> & {
    path?: string;
};

export function WithSlider({ path, children, ...others }: WithSliderProps) {
    const navigate = useNavigate();
    const control = useDisclose(true);

    const handleClose = () => {
        control.close();

        if (path)
            setTimeout(() => {
                navigate(path);
            }, 500);
    };

    return (
        <SlideOver {...control} close={handleClose} {...others}>
            {children}
        </SlideOver>
    );
}
