import { Fragment, PropsWithChildren, useRef } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { cva, VariantProps } from "class-variance-authority";
import { cn } from "@/lib/utils";
import {XMarkIcon} from "@heroicons/react/24/outline";

export const sizeElementVariants = cva(
    [
        "mx-auto transform rounded-xl bg-white shadow-2xl ring-1 ring-black ring-opacity-5 transition-all",
    ],
    {
        variants: {
            size: {
                default: "max-w-xl",
                lg: "max-w-3xl",
                xl: "max-w-5xl",
            },
        },
        defaultVariants: {
            size: "default",
        },
    },
);

type ModalProps = PropsWithChildren<{
    isOpen?: boolean;
    close: () => void;
    onClose?: () => void;
    handleAfterLeave?: () => void;
    dataTestId?: string;
    disableAutoFocus?: boolean;
    noAutoClose?: boolean;
    className?: string;
}> &
    VariantProps<typeof sizeElementVariants>;

export default function Modal({
  children,
  isOpen = false,
  disableAutoFocus,
  handleAfterLeave,
  close,
  onClose,
  className,
  noAutoClose,
  size,
}: ModalProps) {
    const handleClose = () => {
        close();
        onClose?.();
    };
    const dialogRef = useRef<HTMLDivElement>(null);

    return (
        <Transition.Root
            show={isOpen}
            as={Fragment}
            afterLeave={handleAfterLeave}
            appear
        >
            <Dialog
                as="div"
                ref={dialogRef}
                className="relative z-modal"
                onClose={noAutoClose ? () => {} : handleClose}
                initialFocus={disableAutoFocus ? dialogRef : undefined}
            >
                <Transition.Child
                    as={Fragment}
                    enter="ease-out duration-300"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in duration-200"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <div className="fixed inset-0 bg-slate-500 bg-opacity-25 transition-opacity" />
                </Transition.Child>

                <div className="fixed inset-0 z-10 overflow-y-auto p-4 sm:p-6 md:p-20">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 scale-95"
                        enterTo="opacity-100 scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 scale-100"
                        leaveTo="opacity-0 scale-95"
                    >
                        <Dialog.Panel
                            className={cn(
                                sizeElementVariants({ size }),
                                className,
                            )}
                        >
                            <button
                                type="button"
                                className="rounded-md text-gray-700 hover:text-gray-500 focus:outline-none sm:p-1 mt-2 ml-2"
                                onClick={handleClose}
                            >
                                <span className="sr-only">Close panel</span>
                                <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                            {children}
                        </Dialog.Panel>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );
}
