import { cn } from "@/lib/utils";
import { cva, VariantProps } from "class-variance-authority";
import {
    ComponentProps,
    ElementRef,
    Fragment,
    PropsWithChildren,
    ReactNode,
    RefObject,
    forwardRef,
    useContext,
    useState,
    useMemo,
    createContext,
    useRef,
} from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XMarkIcon } from "@heroicons/react/24/outline";

type CloseSlideOverType = () => void;

function emptyFn() {}

type SlideOverContextType = {
    closeSlideOver: CloseSlideOverType;
    openSlideOver: null | CloseSlideOverType;
    footerRef?: null | undefined | RefObject<HTMLDivElement>;
    isOpen?: boolean;
};

const SlideOverContext = createContext<SlideOverContextType>({
    closeSlideOver: emptyFn,
    openSlideOver: null,
    footerRef: null,
    isOpen: false,
});

type ProviderProps = Pick<SlideOverContextType, "footerRef" | "isOpen"> &
    PropsWithChildren<Partial<DiscloseArgs>>;

export function SlideOverContextProvider(props: ProviderProps) {
    const { children, footerRef, close, open, isOpen } = props;

    const value = useMemo(
        () => ({
            closeSlideOver: close ?? emptyFn,
            openSlideOver: open ?? emptyFn,
            isOpen,
            footerRef,
        }),
        [close, open, isOpen, footerRef],
    );

    return (
        <SlideOverContext.Provider value={value}>
            {children}
        </SlideOverContext.Provider>
    );
}

export type DiscloseArgs = {
    isOpen: boolean;
    toggle: () => void;
    close: () => void;
    open: () => void;
};

export function useDisclose(defaultOpen?: boolean): DiscloseArgs {
    const [isOpen, setIsOpen] = useState(defaultOpen ?? false);

    function toggle() {
        setIsOpen((isOpenState) => !isOpenState);
    }

    function close() {
        setIsOpen(false);
    }

    function open() {
        setIsOpen(true);
    }

    return {
        isOpen,
        toggle,
        close,
        open,
    };
}


interface Props
    extends Omit<ComponentProps<"div">, "width" | "title">,
        Partial<DiscloseArgs> {
    title?: ReactNode;
    hasFooter?: boolean;
    width?: string;
    className?: string;
    placement?: SlideOverContentProps["placement"];
}

function SlideOver({
   children,
   title,
   className,
   width = "xl",
   hasFooter = false,
   placement,
   ...others
}: Props) {
    const footerRef: RefObject<HTMLDivElement> = useRef() as never;

    const root = useMemo(
        () => (
            <SlideOver.Root>
                <SlideOver.Dialog>
                    <SlideOver.Overlay />
                    <SlideOver.Content
                        width={width}
                        className={className}
                        placement={placement}
                    >
                        <div
                            className="flex bg-white shadow-xl p-5 justify-center"
                        >
                            <SlideOver.CloseButton placement={placement}/>
                            {title && (
                                    <SlideOver.Title>{title}</SlideOver.Title>
                            )}
                            <div className="flex">{children}</div>
                        </div>
                    </SlideOver.Content>
                </SlideOver.Dialog>
            </SlideOver.Root>
        ),
        [
            children,
            hasFooter,
            title,
            width,
            className,
            placement,
        ],
    );

    return (
        <SlideOverContextProvider footerRef={footerRef} {...others}>
            {root}
        </SlideOverContextProvider>
    );
}

const SlideOverDialog = forwardRef<
    ElementRef<typeof Dialog>,
    ComponentProps<"div">
>(({ children, className }, ref) => {
    const { closeSlideOver } = useContext(SlideOverContext);

    return (
        <Dialog
            ref={ref}
            as="div"
            className={cn("relative z-drawer", className)}
            onClose={closeSlideOver}
        >
            {children}
        </Dialog>
    );
});
SlideOver.Dialog = SlideOverDialog;

function SlideOverRoot({ children }: PropsWithChildren) {
    const { isOpen } = useContext(SlideOverContext);

    return (
        <Transition.Root show={isOpen} appear as={Fragment}>
            {children}
        </Transition.Root>
    );
}
SlideOver.Root = SlideOverRoot;

function SlideOverOverlay() {
    return (
        <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
        >
            <div className="fixed inset-0 bg-slate-800 bg-opacity-75 transition-opacity" />
        </Transition.Child>
    );
}
SlideOver.Overlay = SlideOverOverlay;

const slideOverContentVariants = cva(
    ["pointer-events-none fixed inset-y-0 flex max-w-full"],
    {
        variants: {
            placement: {
                left: "left-0",
                right: "right-0 justify-end",
            },
        },
        defaultVariants: {
            placement: "right",
        },
    },
);

type SlideOverContentProps = VariantProps<typeof slideOverContentVariants> &
    PropsWithChildren<Pick<Props, "width" | "className">>;

function SlideOverContent({
  children,
  className,
  placement = "right",
  width,
}: SlideOverContentProps) {
    return (
        <div className="fixed inset-0 overflow-hidden">
            <div className="absolute inset-0 overflow-hidden">
                <div
                    className={cn(
                        slideOverContentVariants({ placement }),
                        className,
                    )}
                >
                    <Transition.Child
                        as={Dialog.Panel}
                        enter="transform transition ease-in-out duration-300 xl:duration-500"
                        enterFrom={cn({
                            "translate-x-full": placement === "right",
                            "-translate-x-full": placement === "left",
                        })}
                        enterTo="translate-x-0"
                        leave="transform transition ease-in-out duration-300 xl:duration-500"
                        leaveFrom="translate-x-0"
                        leaveTo={cn({
                            "translate-x-full": placement === "right",
                            "-translate-x-full": placement === "left",
                        })}
                        className={cn("pointer-events-auto w-screen", {
                            "max-w-md": width === "md",
                            "max-w-xl":
                                width === "xl",
                        })}
                    >
                        {children}
                    </Transition.Child>
                </div>
            </div>
        </div>
    );
}
SlideOver.Content = SlideOverContent;

const slideOverCloseButtonVariants = cva(["absolute top-[10px] flex z-10"], {
    variants: {
        placement: {
            left: "right-[10px] sm:right-[10px]",
            right: "right-[10px] md:right-[unset] md:left-[10px]",
        },
    },
    defaultVariants: {
        placement: "right",
    },
});

function SlideOverCloseButton({
  placement,
}: VariantProps<typeof slideOverCloseButtonVariants>) {
    const { closeSlideOver } = useContext(SlideOverContext);
    return (
        <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
        >
            <div className={slideOverCloseButtonVariants({ placement })}>
                <button
                    type="button"
                    className="rounded-md text-gray-700 hover:text-gray-500 focus:outline-none sm:p-1"
                    onClick={closeSlideOver}
                >
                    <span className="sr-only">Close panel</span>
                    <XMarkIcon className="h-8 w-8" aria-hidden="true" />
                </button>
            </div>
        </Transition.Child>
    );
}

SlideOver.CloseButton = SlideOverCloseButton;

function SlideOverTitle({
    children,
    ...others
}: ComponentProps<typeof Dialog.Title>) {
    return (
        <Dialog.Title
            className="text-lg font-medium text-slate-900"
            {...others}
        >
            {children}
        </Dialog.Title>
    );
}

SlideOver.Title = SlideOverTitle;

export default SlideOver;
