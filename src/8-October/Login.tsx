import { ChangeEvent, FormEvent, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { login, load_user } from "@/actions/auth";
import { Button } from "@/components/ui/button";
import { Label } from "@/components/ui/label";
import logo from "@/assets/delvin_logo.png";

import { RootState, AppDispatch } from "@/store";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { ExclamationCircleIcon } from "@heroicons/react/20/solid";
import Spinner from "@/components/Spinner";
import { loadUserSuccess } from "@/slices/authSlice";
import Modal from "@/components/ui/modal";
import {WithSlider} from "@/components/ui/withSlider";

function Login() {
  const [isModalOpen, setIsModalOpen] = useState(true);
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const { email, password } = formData;

  const [loginSuccessful, setLoginSuccessful] = useState<boolean | null>(null);
  const [error, setError] = useState<string>("");

  const dispatch: AppDispatch = useDispatch();
  // Replace with the appropriate state shape or selector function
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  );

  const isLoading = useSelector((state: RootState) => state.auth.loading);

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    setFormData({ ...formData, [target.name]: target.value });
  };

  const onSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const result = await dispatch(login(email, password));
    if (result.isSuccessful) {
      setLoginSuccessful(true);
    } else {
      const errorDetail = result.message.includes("No active account found")
        ? "Wrong username or password!"
        : result.message;
      dispatch(loadUserSuccess({ email: email, name: "", id: "" }));
      setLoginSuccessful(false);
      setError(errorDetail);
    }
  };

  if (isAuthenticated) {
    dispatch(load_user());
    return <Navigate to={AUTH_PAGE_URLS.home} />;
  }

  return (
    <>
      <Modal close={()=> {
        setIsModalOpen(!isModalOpen)
      }} isOpen={isModalOpen}>
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto lg:py-0">
          <img className="w-80 mb-3" src={logo} alt="logo" />
          <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Sign in to your account
              </h1>
              {loginSuccessful === false && (
                  <div className="text-red-500 border border-red-500 p-5 rounded-md flex gap-5 items-center">
                    <ExclamationCircleIcon className="w-8" />
                    <p className="">{error}</p>
                  </div>
              )}
              {isLoading && (
                  <div className="mx-auto">
                    <Spinner />
                  </div>
              )}
              <form
                  className="space-y-4 md:space-y-6"
                  onSubmit={(e) => onSubmit(e)}
              >
                <div>
                  <Label htmlFor="email">Your email</Label>
                  <input
                      type="email"
                      name="email"
                      id="email"
                      className="simple-input"
                      placeholder="name@company.com"
                      onChange={(e) => onChange(e)}
                      required
                  />
                </div>
                <div>
                  <label
                      htmlFor="password"
                      className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Password
                  </label>
                  <input
                      type="password"
                      name="password"
                      id="password"
                      placeholder="••••••••"
                      className="simple-input"
                      onChange={(e) => onChange(e)}
                      required
                  />
                </div>
                <div className="flex items-center justify-left">
                  <Link
                      to={AUTH_PAGE_URLS.reset_password}
                      className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
                  >
                    Forgot password?
                  </Link>
                </div>
                <Button type="submit" className="w-full">
                  Sign in
                </Button>
                <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                  Don’t have an account yet?{" "}
                  <Link
                      to={AUTH_PAGE_URLS.signup}
                      className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                  >
                    Sign up
                  </Link>
                </p>
              </form>
            </div>
          </div>
        </div>
      </Modal>

      {/*<WithSlider>*/}
      {/*  <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto lg:py-0">*/}
      {/*    <img className="w-80 mb-3" src={logo} alt="logo" />*/}
      {/*    <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">*/}
      {/*      <div className="p-6 space-y-4 md:space-y-6 sm:p-8">*/}
      {/*        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">*/}
      {/*          Sign in to your account*/}
      {/*        </h1>*/}
      {/*        {loginSuccessful === false && (*/}
      {/*            <div className="text-red-500 border border-red-500 p-5 rounded-md flex gap-5 items-center">*/}
      {/*              <ExclamationCircleIcon className="w-8" />*/}
      {/*              <p className="">{error}</p>*/}
      {/*            </div>*/}
      {/*        )}*/}
      {/*        {isLoading && (*/}
      {/*            <div className="mx-auto">*/}
      {/*              <Spinner />*/}
      {/*            </div>*/}
      {/*        )}*/}
      {/*        <form*/}
      {/*            className="space-y-4 md:space-y-6"*/}
      {/*            onSubmit={(e) => onSubmit(e)}*/}
      {/*        >*/}
      {/*          <div>*/}
      {/*            <Label htmlFor="email">Your email</Label>*/}
      {/*            <input*/}
      {/*                type="email"*/}
      {/*                name="email"*/}
      {/*                id="email"*/}
      {/*                className="simple-input"*/}
      {/*                placeholder="name@company.com"*/}
      {/*                onChange={(e) => onChange(e)}*/}
      {/*                required*/}
      {/*            />*/}
      {/*          </div>*/}
      {/*          <div>*/}
      {/*            <label*/}
      {/*                htmlFor="password"*/}
      {/*                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"*/}
      {/*            >*/}
      {/*              Password*/}
      {/*            </label>*/}
      {/*            <input*/}
      {/*                type="password"*/}
      {/*                name="password"*/}
      {/*                id="password"*/}
      {/*                placeholder="••••••••"*/}
      {/*                className="simple-input"*/}
      {/*                onChange={(e) => onChange(e)}*/}
      {/*                required*/}
      {/*            />*/}
      {/*          </div>*/}
      {/*          <div className="flex items-center justify-left">*/}
      {/*            <Link*/}
      {/*                to={AUTH_PAGE_URLS.reset_password}*/}
      {/*                className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"*/}
      {/*            >*/}
      {/*              Forgot password?*/}
      {/*            </Link>*/}
      {/*          </div>*/}
      {/*          <Button type="submit" className="w-full">*/}
      {/*            Sign in*/}
      {/*          </Button>*/}
      {/*          <p className="text-sm font-light text-gray-500 dark:text-gray-400">*/}
      {/*            Don’t have an account yet?{" "}*/}
      {/*            <Link*/}
      {/*                to={AUTH_PAGE_URLS.signup}*/}
      {/*                className="font-medium text-primary-600 hover:underline dark:text-primary-500"*/}
      {/*            >*/}
      {/*              Sign up*/}
      {/*            </Link>*/}
      {/*          </p>*/}
      {/*        </form>*/}
      {/*      </div>*/}
      {/*    </div>*/}
      {/*  </div>*/}
      {/*</WithSlider>*/}
    </>
  );
}

export default Login;
