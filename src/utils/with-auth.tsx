import {
	ComponentType,
	FC,
	PropsWithChildren,
	useContext,
	useEffect, useState,
} from "react";
import { useLocation, useNavigate } from "react-router-dom";
import AccountContainer from "src/containers/AccountContainer";
import { PAGE_URLS } from "src/configs/pages-urls";
import { useAuth } from "src/services/oidc-exporter";
import AccountContext from "src/contexts/account";
//
// export enum Roles {
// 	EditProjects = "edit-projects",
// 	ViewProjects = "view-projects",
// }
//
// export enum PortalFeatures {
// 	projects = "projects",
// 	newProject = "newProject",
// 	settings = "settings",
// }
//
// const hasPermission = (profile: object, roles: string[]): boolean => {
// 	return roles.some((role) => profile.roles.includes(role))
// };
//
// const featureAccessMap: Record<PortalFeatures, PortalFeatures[]> = {
// 	[PortalFeatures.projects]: [Roles.ViewProjects, Roles.EditProjects],
// 	[PortalFeatures.newProject]: [Roles.EditProjects],
// 	[PortalFeatures.settings]: [Roles.EditProjects],
// };
//
// export const hasAccessTo = ({
// 	profile,
// 	featureName
// }: {
// 	profile?: Object;
// 	featureName: PortalFeatures,
// }) => {
// 	try {
// 		if (!profile) return false;
// 		if (!featureAccessMap[featureName]) return false;
// 		return hasPermission(profile, featureAccessMap[featureName]);
// 	} catch (error) {
// 		return false;
// 	}
// };
//
// function WithAccountAuth({
// 	featureName,
// 	children,
// }: PropsWithChildren<{ featureName: PortalFeatures }>) {
// 	const {
// 		account,
// 		isLoading,
// 		error: accountError,
// 	} = useContext(AccountContext);
// 	const { user } = useAuth();
// 	const navigate = useNavigate();
// 	const location = useLocation();
//
// 	useEffect(() => {
// 		if (
// 			featureName &&
// 			!hasAccessTo({
// 				featureName,
// 				profile: user,
// 				account,
// 			})
// 		) {
// 			navigate(`/${PAGE_URLS.notAuthorized.path}`, {
// 				state: { redirectedFrom: location.pathname },
// 			});
// 		}
// 	}, [account, featureName, location.pathname, navigate, user]);
//
// 	if (isLoading || !user) {
// 		return (
// 			<div className="loading">Loading screen</div>
// 		);
// 	}
//
// 	if (accountError) {
// 		return (
// 			<div className="error">Error screen</div>
// 		);
// 	}
//
// 	return children;
// }

const withAuth = <P extends object>(
	Component: ComponentType<P>
): FC<P> =>
	function WithAuthentication({
		...props
	}: P): JSX.Element {
	    // smth to take user from
		const { isAuthenticated, user } = useAuth();
		const navigate = useNavigate();
		const location = useLocation();
		const [loading, setLoading] = useState(false);

		useEffect(() => {
			if (!isAuthenticated) {
				navigate(`/${PAGE_URLS.signIn.path}`);
			}
			// send request
		}, [
			isAuthenticated,
			navigate,
			location,
			user,
		]);

		if (isAuthenticated) {
			return <Component {...(props as P)} />
		}

		if (!isAuthenticated && loading) {
			return <div>Loading</div>
		}

		return <div>Sign in</div>
	};

export default withAuth;
