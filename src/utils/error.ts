import * as Sentry from "@sentry/react";

export const throwWithSentry: typeof Sentry.captureException = (
    error,
    opts,
) => {
    Sentry.captureException(error, opts ?? { level: "info" });
    throw error;
};

type HTTPError = { status: number, response: object };
export function handleServerError(error: Error | HTTPError) {
    console.error({ serverError: error });

    if (
        (error as HTTPError)?.response &&
        (error as HTTPError).status >= 500
    ) {
        Sentry.captureException(error);
    }
}
