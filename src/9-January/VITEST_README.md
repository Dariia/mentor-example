# Testing react with Vitest

## Setup

 - nvm use 18
 - update playwright (?)
 - `npm i -D jsdom vitest` so vitest could use html
 - Add vitest.config.js

```js
import { defineConfig } from 'vitest/config'

export default defineConfig({
    test: {
        globals: true,
        environment: 'jsdom',
        css: true,
        mockReset: true,
        restoreMocks: true,
        clearMocks: true,
        setupFiles: ["./test/setup.ts"],
        include: ['./src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
        exclude: [
            '**/node_modules/**',
            '**/dist/**',
            '**/public/**',
            '**/.{idea,git,cache,output,temp}/**',
            './src/config/**',
            './src/__tests__/views/**',
        ],
    },
});

```

- `npm add -D @testing-library/react @testing-library/jest-dom` for testing react components with react library
- `npm add -D @testing-library/user-event` for interactions with react components
- `npm i @types/jest jest-environment-jsdom`

- Create setup.ts in test folder

```ts
import { expect, afterEach } from "vitest";
import { cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import * as matchers from '@testing-library/jest-dom/matchers';

expect.extend(matchers);

afterEach(() => {
    cleanup();
});
```
-  package.json `"test": "vitest"`


## Simple test for Loading component

```tsx
import Loading from "./Loading";
import { screen, render } from "@testing-library/react";
import { describe, it } from "vitest";

const renderComponent = () =>
    render(<Loading/>);

describe("Loading", () => {
    beforeEach(() => {
        renderComponent();
    });

    it("renders text", async () => {
        expect(screen.getByText("Loading...")).toBeInTheDocument();
    });
});

```

