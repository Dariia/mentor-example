/* eslint-disable react/no-unstable-nested-components */
import { PropsWithChildren, Suspense } from "react";
import { FolderPlusIcon } from "@heroicons/react/24/outline";
import { ErrorBoundary } from "react-error-boundary";
import { QueryErrorResetBoundary } from "react-query";
import ErrorState from "@/components/ErrorState";
import Loading from "@/components/Loading";
import { FeedbackVariantsProps } from "@/components/feedbackVariants";
import { EmptyState } from "@/components/EmptyState";

export type ErrorStateType = Array<Error | null | undefined> | Error | null;
export function hasError(error: ErrorStateType | undefined) {
    if (Array.isArray(error)) {
        return error.length > 0;
    }

    return !!error;
}

export type ErrorBoundaryWrapperProps = PropsWithChildren<{
    isLoading?: boolean;
    error?: ErrorStateType;
    errorTitle?: string;
    errorDescription?: string;
    isEmptyState?: boolean;
    className?: string;
    emptyTitle?: string;
    emptyDescription?: string;
    emptyIcon?: typeof FolderPlusIcon;
    emptyTo?: string;
    variant?: FeedbackVariantsProps["variant"];
}>;

export function ErrorBoundaryWrapper({
    isLoading = false,
    error: errorProp,
    errorTitle,
    errorDescription,
    isEmptyState = false,
    children,
    emptyTitle,
    emptyDescription,
    emptyIcon = FolderPlusIcon,
    emptyTo,
    variant,
    className,
}: ErrorBoundaryWrapperProps) {
    if (isLoading) {
        return <Loading />;
    }

    if (hasError(errorProp)) {
        return (
            <ErrorState
                error={errorProp}
                title={errorTitle}
                description={errorDescription}
                variant={variant}
            />
        );
    }

    if (isEmptyState) {
        return (
            <EmptyState
                title={emptyTitle}
                description={emptyDescription}
                variant={variant}
                Icon={emptyIcon}
                className={className}
                to={emptyTo}
            />
        );
    }

    return (
        <QueryErrorResetBoundary>
            {({ reset }) => (
                <ErrorBoundary
                    fallbackRender={({ error, resetErrorBoundary }) => (
                        <ErrorState
                            error={error as Error}
                            resetErrorBoundary={resetErrorBoundary}
                            variant={variant}
                        >{children}</ErrorState>
                    )}
                    onReset={reset}
                >
                    <Suspense fallback={<Loading />}>{children}</Suspense>
                </ErrorBoundary>
            )}
        </QueryErrorResetBoundary>
    );
}
