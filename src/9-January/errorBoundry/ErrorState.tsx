import { Link } from "react-router-dom";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { buttonVariants } from "@/components/ui/button";
import { ErrorStateType } from "@/components/ErrorBoundaryWrapper";
import { ComponentProps } from "react";
import {feedbackVariants, FeedbackVariantsProps} from "@/components/feedbackVariants";
import {cn} from "@/lib/utils";

export type ErrorStateProps = {
  error?: ErrorStateType;
  errorKey?: string;
  resetErrorBoundary?: () => void;
} & { title?: string; description?: string } & ComponentProps<"div">  & FeedbackVariantsProps;

export default function ErrorState({
   title,
   description,
   className,
   error,
   errorKey,
   resetErrorBoundary,
   children,
    variant,
   ...others
}: ErrorStateProps) {
  return (
    <div
        data-testid="empty-state"
        className={cn(
            feedbackVariants({ variant }),
            className,
        )}
        {...others}
    >
      <div className="not-found-container">
        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-4xl dark:text-white py-5">
          { title || "Oops! The page you were looking for does not exist." }
        </h1>

        {description && <p className="py-5">{ description }</p>}
        <Link
          to={AUTH_PAGE_URLS.home}
          className={buttonVariants({ variant: "default" })}
        >
          Back to Home{" "}
        </Link>
        {resetErrorBoundary && (
            <div>
                <button onClick={() => resetErrorBoundary()}>
                    Retry
                </button>
            </div>
        )}
      </div>
    </div>
  );
}
