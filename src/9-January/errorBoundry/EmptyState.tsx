import { Link } from "react-router-dom";
import { buttonVariants } from "@/components/ui/button";
import { cn } from "@/lib/utils";
import { FolderPlusIcon } from "@heroicons/react/24/outline";
import {
  feedbackVariants,
  FeedbackVariantsProps,
} from "@/components/feedbackVariants";
import { ComponentProps } from "react";

export type EmptyStateProps = {
  Icon?: typeof FolderPlusIcon;
  iconClassName?: string;
} & {
  title?: string;
  description?: string;
  to?: string;
} & ComponentProps<"div"> &
    FeedbackVariantsProps;

export function EmptyState({
   title,
   Icon,
   description,
   variant = "fit",
   to,
   className,
   ...others
}: EmptyStateProps) {
  const IconComponent = Icon || FolderPlusIcon;

  function content() {
    return (
        <div
            data-testid="empty-state"
            className={cn(
                feedbackVariants({ variant }),
                className,
                "bg-background",
                to && "hover:border-slate-400",
            )}
            {...others}
        >
          <IconComponent className="h-20 w-20 text-indigo-400" />
          <span className="block text-xl font-medium text-slate-900 mt-4">
					{title || "List is empty. Please add some items to the list."}
				</span>
          {description && (
              <span className="block max-w-lg text-center text-base font-medium text-slate-700 p-4">
                  {description}
              </span>
          )}
        </div>
    );
  }
  return to ? (
      <Link
          to={to}
          className={buttonVariants({ variant: "default" })}
      >
        Create New Item {" "}
      </Link>
  ) : (
      <>{content()}</>
  );
}
