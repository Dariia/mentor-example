import Loading from "./Loading";
import { screen, render } from "@testing-library/react";

const renderComponent = () =>
    render(<Loading/>);

describe("Loading", () => {
    beforeEach(() => {
        renderComponent();
    });

    it("renders text", async () => {
        expect(screen.getByText("Loading...")).toBeInTheDocument();
    });
});
