import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    globals: true,
    environment: 'jsdom',
    css: true,
    mockReset: true,
    restoreMocks: true,
    clearMocks: true,
    setupFiles: ["./test/setup.ts"],
    include: ['./src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
    exclude: [
      '**/node_modules/**',
      '**/dist/**',
      '**/public/**',
      '**/.{idea,git,cache,output,temp}/**',
      './src/config/**',
      './src/__tests__/views/**',
    ],
  },
});
