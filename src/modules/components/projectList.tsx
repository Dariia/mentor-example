import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useQuery } from "@tanstack/react-query";
import { Card, Title, Subtitle } from "@tremor/react";
import Navbar from "@/components/Navbar";
import NewProjectSheet from "../components/NewProjectSheet";
import { RootState } from "@/store";
import { setProject } from "../../../slices/projectSlice";
import { projectQuery } from "../projectsModule/configs/projects.query-factory";

function ProjectListComponent({ projectListData }: {
    projectListData: object
}) {
  //  const [projects, setProjects] = useState<Project[]>([]);

    const userId = useSelector((state: RootState) => state.auth.user?.id) ?? "";
    const dispatch = useDispatch();
    function handleProjectClick(project: any) {
        dispatch(setProject(project));
    }

    // TODO: to use on each route <WithAuth>
    // const isAuthenticated = useSelector(
    //     (state: RootState) => state.auth.isAuthenticated
    // );

    // NOTE: now we moved it to projectQuery.list
    // useEffect(() => {
    //     const fetchProjects = async () => {
    //         try {
    //             const response = await axiosInstance.get(
    //                 `/api/projects/list/?owner_id=${userId}`
    //             );
    //             setProjects(response.data);
    //         } catch (error: any) {
    //             console.error("There was an error fetching the projects!", error);
    //         }
    //     };
    //
    //     if (userId) {
    //         fetchProjects();
    //     }
    // }, [userId]);


    // TODO: to use on each route <WithAuth>
    // if (!isAuthenticated) {
    //     return <Navigate to={AUTH_PAGE_URLS.login} />;
    // }


    // QUERY Example
    const { data, isLoading, error } = useQuery(projectQuery.list());

    return (
        <div>
            <Navbar withLogo={true} header={"Projects"} />
            <div className="flex flex-row justify-between max-w-md mx-auto mt-20">
                <Title> Your Projects </Title>
                <NewProjectSheet />
            </div>
            {projectListData.map((project) => (
                <Link
                    to={`/project/${project.id}/home`}
                    key={project.id}
                    onClick={() => handleProjectClick(project)}
                >
                    <Card className="max-w-md mx-auto mt-3" key={project.id}>
                        <Subtitle> {project.name} </Subtitle>
                    </Card>
                </Link>
            ))}
        </div>
    );
}

export default ProjectListComponent;
