import { useMutation, useQueryClient } from "@tanstack/react-query";
import {projectKeys} from "../configs/projects.query-factory";
import {axiosInstance} from "../../../api";

const useProjectCreate = () => {
	const queryClient = useQueryClient();

    // Types: <[what returns], [error], [request]>
	return useMutation<Object, Error, Object>({
		mutationFn: (payload) => {
			if (!axiosInstance) {
				throw new Error("No axiosInstance");
			}
			return axiosInstance?.post(`/some_url/`)(payload, payload);
		},
		onSuccess: () => {
			queryClient
				.invalidateQueries({
					queryKey: projectKeys.lists(),
				})
				.catch(console.error);
		},
	});
};

export default useProjectCreate;
