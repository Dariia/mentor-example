import { useMutation, useQueryClient } from "@tanstack/react-query";
import {projectKeys} from "../configs/projects.query-factory";
import {axiosInstance} from "../../../api";

const useProjectsUpdate = (id?: number) => {
	const queryClient = useQueryClient();

	// Types: <[what returns], [error], [request]>
	return useMutation<Object, Error, Object>({
		mutationFn: (payload) => {
			if (!id) {
				throw new Error("No id");
			}
			if (!axiosInstance) {
				throw new Error("No axiosInstance");
			}
			return axiosInstance?.put(
				`/some_url/`
			)(id, payload);
		},
		onSuccess: (result) => {
			queryClient.setQueryData(projectKeys.detail(result.id), result);
			queryClient
				.invalidateQueries({
					queryKey: projectKeys.lists(),
				})
				.catch(console.error);
			queryClient
				.invalidateQueries({
					queryKey: projectKeys.detail(result.id),
				})
				.catch(console.error);
		},
	});
};

export default useProjectsUpdate;
