import { useSelector } from "react-redux";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { RootState } from "@/store";
import { getProjectHomeData } from "@/actions/project/project_home";
import useProjectCreate from "./hooks/use-project-create";
import useProjectsUpdate from "./hooks/use-projects-update";

function ProjectDetails() {
    const userName = useSelector((state: RootState) => state.auth.user?.name);

    const routeParams = useParams();
    const projectId = routeParams.projectId!;

    const { data: projectHomeData, isLoading: isHomeDataLoading } = useQuery(
        ["chartData", projectId],
        () => getProjectHomeData(projectId),
    );


    // Create project hook
    const postProject = useProjectCreate();
    // Update project hook
    const updateProject = useProjectsUpdate(parseInt(projectId, 10));


    const handleCreate: Object = (
        formData,
    ) => {
        // some manipulations with form data (formData) if needed
        const payload = formData;
        postProject.mutate(payload, {
            onSuccess: () => {
                // Show some toast for example
                return true;
            },
            onError: () => {
                // Show some toast for example
                return true;
            },
            onSettled: () => {
                // do smth
            },
        });
    }

    const handleUpdate: Object = (
        formData,
    ) => {
        // some manipulations with form data (formData) if needed
        const payload = formData;
        updateProject.mutate(payload, {
            onSuccess: () => {
                // Show some toast for example
                return true;
            },
            onError: () => {
                // Show some toast for example
                return true;
            },
        });
    }


    return (
        <form className="flex-1 flex-col" onSubmit={handleCreate}>
            <div className="text-left my-5 mx-2">
                <h1 className="text-3xl"> Project Details </h1>
            </div>
            { projectHomeData }
            <button type="submit">Create project</button>
        </form>
    );
}

export default ProjectDetails;
