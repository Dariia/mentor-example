import { QueryClient, queryOptions } from "@tanstack/react-query";
import { projectQuery } from "./configs/projects.query-factory";
import { axiosInstance as AxiosInstance } from "../api";

export const projectListLoader = (
    qc: QueryClient,
    axiosInstance: typeof AxiosInstance | null,
    userId: number,
    projectId: string | undefined,
) => {
    try {
        return Promise.all([
            qc.ensureQueryData(
                queryOptions(projectQuery.list(axiosInstance, userId)),
            ),
            qc.ensureQueryData(
                queryOptions(projectQuery.detail(axiosInstance, projectId)),
            ),
        ]);
    } catch (err) {
      // Do smth
    }
};
