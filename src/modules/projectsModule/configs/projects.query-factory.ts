import { axiosInstance as AxiosInstance } from "../../../api";

export const projectKeys = {
	all: ["project"] as const,
	lists: () => [...projectKeys.all, "list"] as const,
	list: (filters: string) => [...projectKeys.lists(), filters] as const,
	details: () => [...projectKeys.all, "detail"] as const,
	detail: (id: string) => [...projectKeys.details(), id] as const,
};

export const projectQuery = {
	list: (axiosInstance: typeof AxiosInstance | null, userId?: number) =>
		({
			queryKey: projectKeys.list(String(userId)),
			queryFn: async () => {
				// try {
					return axiosInstance?.get(
						`/api/projects/list/?owner_id=${userId}`
					);
					// setProjects(response.data);
				// } catch (error: any) {
				// 	console.error("There was an error fetching the projects!", error);
				// }
			},
			enabled: !!axiosInstance && !!userId,
			staleTime: 0,
		}) as const,
	detail: (axiosInstance: typeof AxiosInstance | null, projectId: string | undefined) =>
		({
			queryKey: projectKeys.detail(projectId!),
			queryFn: async () => {
				return axiosInstance?.get(
					`/some_url/`
				);
			},
			enabled: !!axiosInstance && !!projectId,
			staleTime: 0,
		}) as const,
};
