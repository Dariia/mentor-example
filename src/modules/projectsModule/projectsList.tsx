import { useLoaderData } from "react-router-dom";
import { projectListLoader } from "./loaders";
import ProjectListComponent from "../components/projectList";
import withAuth, {PortalFeatures} from "../../utils/with-auth";

export function ProjectListPage() {
    const [
        projectListData,
        // if there will be some other request
       someOtherData,
    ] = useLoaderData() as Awaited<ReturnType<typeof projectListLoader>>; // type is returnType of loader

    return (
        <ProjectListComponent projectListData={projectListData} />
    );
}

const ProjectListPageWithAuth = withAuth(ProjectListPage);

export function Component() {
    return <ProjectListPageWithAuth featureName={PortalFeatures.settings}/>;
}

Component.displayName = "projectList";
