
// *********** Issues *********** //
// https://github.com/reduxjs/redux-toolkit/issues/3692

// cache
// onSuccess/onError
// cached results reset - https://github.com/reduxjs/redux-toolkit/issues/3692#issuecomment-1724770561
// turn off refetchOnFocus and refetchOnReconnect globally and opt-in per hook -



// *********** pagination issues *********** //
// prefetch pages - https://github.com/reduxjs/redux-toolkit/issues/3692#issuecomment-1846345838
// scroll - https://github.com/reduxjs/redux-toolkit/discussions/3174#discussioncomment-6948741


// *********** complex queries (like useQueries) *********** //
// combining queries looks hacky - https://github.com/reduxjs/redux-toolkit/discussions/1171



// *********** Transform response *********** //
// https://redux-toolkit.js.org/rtk-query/usage/customizing-queries#examples---transformresponse



// *********** Transform error *********** //
// https://redux-toolkit.js.org/rtk-query/usage/customizing-queries#customizing-query-responses-with-transformerrorresponse



// *********** nice simple slice example ***********//
// const profileApi = api.injectEndpoints({
//     endpoints: (build) => ({
//         getProfile: build.query<Profile, undefined>({ query: () => "/profile" }),
//         updateProfile: build.mutation<Profile, Partial<Profile>>({
//             query: (profile) => ({
//                 url: "/profile",
//                 method: "PATCH",
//                 body: profile,
//             }),
//         }),
//     }),
// });



// *********** Examples with structure *********** //
// https://github.com/rtk-incubator/rtk-query/tree/main/examples/react/src/app/services



