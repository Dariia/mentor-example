import { Outlet, useLocation, useParams } from "react-router-dom";
import { useEffect, useState } from "react";

import Sidebar from "@/components/layout/Sidebar";
import { useSelector } from "react-redux";
import { AppDispatch, RootState } from "@/store";
import { useDispatch } from "react-redux";
import { fetchProjects, retrieveChatbot } from "@/lib/api";
import { setAllUserProjects, setChatbot, setProject } from "@/slices/projectSlice";
import { Project } from "@/typings/project";
import Loading from "@/components/Loading";
import { sleep } from "@/lib/utils";

function ProjectPage() {
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated
  );
  const user = useSelector(
    (state: RootState) => state.auth.user
  );
  const project = useSelector(
    (state: RootState) => state.project
  );

  const dispatch: AppDispatch = useDispatch();
  const location = useLocation();
  const [loading, setLoading] = useState(true);
  const routeParams = useParams();
  const projectId = routeParams.projectId!;

  useEffect(() => {
    const initializeData = async () => {
      try {
        // if (!isAuthenticated) {
        //   await sleep(1000);
        //   await dispatch(checkAuthenticated());
        // }
        //
        // if (!user) {
        //   await dispatch(load_user());
        // }

        if (user && !project.id) {
          setLoading(true);
          await sleep(1000);
          const projects = await fetchProjects(user.id);
          dispatch(setAllUserProjects(projects));
          const currProject = projects.find((proj: Project) => proj.id === projectId);
          dispatch(setProject(currProject));
          const chatbot = await retrieveChatbot(projectId);
          dispatch(setChatbot(chatbot));
        }
      } catch (error) {
        console.error("Error initializing data:", error);
      } finally {
        setLoading(false);
      }
    };

    initializeData();
  }, [user, project, dispatch]);

  if (loading) {
    return (
      <Loading />
    );
  }
  //
  // if (!isAuthenticated) {
  //   return <Navigate to={AUTH_PAGE_URLS.login} />;
  // }

  if (isAuthenticated && user && project.id) {
    return (
        <div className="flex h-screen border-collapse overflow-hidden">
            <Sidebar />
            <main className="flex-1 overflow-y-auto overflow-x-hidden pb-1 px-10">
                <Outlet />
            </main>
        </div>
  )}

  // return (
  //   <Loading />
  // );

};

export default ProjectPage;
