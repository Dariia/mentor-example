import { checkAuthenticated, load_user } from "@/actions/auth";
import { AUTH_PAGE_URLS } from "@/routes/pageUrls";
import { AppDispatch, RootState } from "@/store";
import {
    ComponentType,
    ReactElement,
    useEffect,
    useState, } from "react";
import { useDispatch, useSelector } from "react-redux";
import {useLocation, useNavigate} from "react-router-dom";
import {sleep} from "@/lib/utils.ts";
import Loading from "@/components/Loading.tsx";

function withAuth<P extends object>(
    Component: ComponentType<P>
  ): (props: P) => ReactElement  {
    return function WithAuthentication(props: P): ReactElement  {
      const isAuthenticated = useSelector(
        (state: RootState) => state.auth.isAuthenticated
      );
      const navigate = useNavigate();
      const user = useSelector(
          (state: RootState) => state.auth.user
      );
      const dispatch: AppDispatch = useDispatch();
      const location = useLocation();
      const [loading, setLoading] = useState(false);

      useEffect(() => {
        const checkAuth = async () => {
          try {
            if (!isAuthenticated || !user) {
              setLoading(true);

              if (!isAuthenticated) {
                await sleep(1000);
                const isAuthRevalidated = await dispatch(checkAuthenticated());

                if (!isAuthRevalidated) {
                  // when to navigate?
                  navigate(`/${AUTH_PAGE_URLS.login}`)
                }
              } else if (!user) {
                const isUserLoaded = await dispatch(load_user());

                // what to do here?
              }
            }
          } catch (error) {
            console.error("Error initializing data:", error);
          } finally {
            setLoading(false);
          }
        };

        checkAuth();
        // dispatch ??
      }, [isAuthenticated, dispatch, location.pathname, user]);

      if (loading) {
        return (<Loading />);
      }

    //  if (isAuthenticated && user) {
        return <Component {...(props as P)} />;
    //  }
    };
  }

  export default withAuth;
