import NewProjectPage from "@/layouts/NewProjectPage";
import ChatbotConfig from "@/modules/chatbot_config/pages/ChatbotConfig";
import ChatPage from "@/modules/conversations/pages/ChatPage";
import Conversations from "@/modules/conversations/pages/Conversations";
import GuardrailResponses from "@/modules/guardrails/pages/GuardrailResponses";
import ProjectHome from "@/modules/project_home/pages/ProjectHome";
import ProjectSettings from "@/modules/project_settings/pages/ProjectSettings";
import Tags from "@/modules/tags/pages/Tags";
import Understanding from "@/modules/understanding/pages/Understanding";
import ChatWithAssistant from "@/modules/chat_with_assistant/pages/ChatWithAssistant";

import { PROJECT_PAGE_URLS } from "./pageUrls";
import withAuth from "@/lib/with-auth.tsx";

const NewProjectPageWithAuth = withAuth(NewProjectPage);

export const ROUTES = [
  {
    path: PROJECT_PAGE_URLS.project.path,
    element: <NewProjectPageWithAuth />,
    children: [
      {
        path: PROJECT_PAGE_URLS.project.children.home,
        element: <ProjectHome />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.conversations,
        element: <Conversations />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.chat_with_assistant,
        element: <ChatWithAssistant />,
      },
      { path: PROJECT_PAGE_URLS.project.children.tags, element: <Tags /> },
      {
        path: PROJECT_PAGE_URLS.project.children.chat,
        element: <ChatPage />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.settings,
        element: <ProjectSettings />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.chatbot_config,
        element: <ChatbotConfig />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.understanding,
        element: <Understanding />,
      },
      {
        path: PROJECT_PAGE_URLS.project.children.guardrail_responses,
        element: <GuardrailResponses />,
      },
    ],
  },
];
